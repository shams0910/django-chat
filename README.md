##Welcome to Django Chat

###Features:

- [x] Sign up and Login
- [x] Sending files and messages to groups

###Setup
To setup project locally you need docker and docker-compose

Run the project 
```
docker-compose up
```

### Usage
Please go to http://127.0.0.1:8000 if you are using locally.\
The project is also available online in http://18.159.195.155/

You will be redirected to Login Page. If you have account then login. If you dont have account got to signup
and go through registration process.

Once successfully logged in, you will be on page with chat rooms. \
You can select any group and send messages and file to that groups.

To test if websockets are working fine you can open another tab of the browser **incognito**(it is required
because you will have another session)

And send message being from one tab, and then see the message you sent in incognito tab.

Thank you

