import json
from enum import Enum
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer

from core.models import ChatRoom, ChatMessage


MSG_TYPE_TEXT_MESSAGE = 'text_message'
MSG_TYPE_FILE_MESSAGE = 'file_message'


class ChatConsumer(WebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.chat_room_ids = ChatRoom.objects.values_list('id', flat=True)

    def connect(self):
        self.accept()
        for i in self.chat_room_ids:
            async_to_sync(self.channel_layer.group_add)(f'chat_room_{i}', self.channel_name)

    def disconnect(self, close_code):
        for i in self.chat_room_ids:
            async_to_sync(self.channel_layer.group_add)(f'chat_room_{i}', self.channel_name)

    def _text_message(self, data):
        room_id = data.get('room_id')
        message = data.get('message')
        user = self.scope['user']
        data['sender_id'] = user.id
        data['sender_username'] = user.username

        ChatMessage.objects.create(room_id=room_id, user=user, message=message)
        async_to_sync(self.channel_layer.group_send)(
            f'chat_room_{room_id}',
            {
                "type": "text_message",
                "data": data,
            },
        )

    def text_message(self, event):
        data = json.dumps(event['data'])
        self.send(text_data=data)

    def file_message(self, event):
        print(event['data'], 'from file_message')

    def receive(self, text_data):
        text_data = json.loads(text_data)
        msg_type: str = text_data.get('type')
        print(text_data)
        if msg_type == MSG_TYPE_TEXT_MESSAGE:
            self._text_message(text_data)



    def chat_message(self, event):
        self.send(text_data=event['text'])

