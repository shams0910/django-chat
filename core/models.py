from django.db import models
from django.contrib.auth.models import User


class ChatRoom(models.Model):
    name = models.CharField(max_length=33)


class ChatMessage(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    room = models.ForeignKey('ChatRoom', on_delete=models.CASCADE, related_name='messages')
    message = models.TextField()

