from django.urls import path
from . import views

urlpatterns = [
    path('index', views.index, name='index'),
    path('chat/<str:room_name>/', views.room, name='room'),
    path('signup', views.sign_up, name='signup'),
    path('login', views.log_in, name='login'),
    path('', views.log_in, name='login'),
    path('logout', views.log_out, name='logout'),
    path('upload_file', views.upload_file, name='upload_file')
]
