from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.http import JsonResponse
from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required

from core.models import ChatRoom


# Create your views here.
@login_required
def index(request):
    chat_rooms = ChatRoom.objects.prefetch_related('messages')
    return render(request, 'core/index.html', {'chat_rooms': chat_rooms})


def room(request, room_name):
    return render(request, 'core/room.html', {
        'room_name': room_name
    })


def upload_file(request):
    if request.method == 'POST':
        room_id = request.POST.get('room_id')
        layer = get_channel_layer()
        print(layer, room_id)
        async_to_sync(layer.group_send)(
            f'chat_room_{room_id}',
            {
                'type': 'file_message',
                'data': 'hello'
            }
        )
        return JsonResponse({'message': 'ok'}, status=200)


def sign_up(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
        else:
            context = {
                'form': form
            }
            return render(request, 'core/signup.html', context)
    else:
        form = UserCreationForm()
        context = {
            'form': form
        }
        return render(request, 'core/signup.html', context)


def log_in(request):
    form = AuthenticationForm()
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            login(request, form.get_user())
            return redirect('index')
        else:
            print(form.errors)
    return render(request, 'core/login.html', {'form': form})


def log_out(request):
    logout(request)
    return redirect(reverse('login'))
